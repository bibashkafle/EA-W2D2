package com.sid1;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@ComponentScan
@Configuration
public class App {

    public static void main(String[] args){
        ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("springconfig.xml");
        Greeting greetingService = context.getBean("greetingService", Greeting.class);
        greetingService.sayHello();
    }
}